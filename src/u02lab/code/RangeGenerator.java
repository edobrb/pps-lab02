package u02lab.code;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop leads to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and should give false, at the end it gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator extends FixedLengthGenerator {

    private final int start;
    private int index = 0;
    public RangeGenerator(final int start, final int stop){
        super(stop - start + 1);
        if (stop < start) {
            throw new IllegalArgumentException("'start' should be less equal than 'stop'");
        }
        this.start = start;
        this.index = 0;
    }

    @Override
    public Integer getNext() {
        return this.start + index++;
    }

    @Override
    public  void reset() {
        super.reset();
        this.index = 0;
    }
}

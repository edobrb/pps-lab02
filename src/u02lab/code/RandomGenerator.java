package u02lab.code;

import java.util.Random;

/**
 * Step 2
 *
 * Now consider a RandomGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (RandomGenerator vs RangeGenerator), using patterns as needed
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator extends FixedLengthGenerator {

    final Random random;
    final long seed;
    public RandomGenerator(final int n){
        super(n);
        this.random = new Random();
        this.seed = random.nextLong();
        this.random.setSeed(seed);
    }

    public RandomGenerator(final int n, final long seed){
        super(n);
        this.seed = seed;
        this.random = new Random(seed);
    }

    @Override
    public Integer getNext() {
        return random.nextInt(2);
    }

    @Override
    public  void reset(){
        super.reset();
        random.setSeed(this.seed);
    }

}

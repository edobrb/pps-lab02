package u02lab.code;

public abstract class SequenceGeneratorFactory {

    public abstract SequenceGenerator newRandomSequence(final int sequenceLength);
    public abstract SequenceGenerator newRangeSequence(final int start, final int stop);

}

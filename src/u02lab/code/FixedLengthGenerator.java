package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class FixedLengthGenerator implements SequenceGenerator {

    private final int length;
    private int currentIndex;

    public FixedLengthGenerator(final int length){
        this.length = length;
        this.currentIndex = 0;
    }

    public abstract Integer getNext();

    @Override
    public Optional<Integer> next() {
        Optional<Integer> nextValue;
        if(this.isOver()){
            nextValue = Optional.empty();
        } else {
            nextValue = Optional.of(this.getNext());
            this.currentIndex++;
        }
        return nextValue;
    }

    @Override
    public void reset() {
        this.currentIndex = 0;
    }

    @Override
    public boolean isOver() {
        return this.currentIndex >= this.length;
    }

    @Override
    public List<Integer> allRemaining() {
        final List<Integer> remaining = new ArrayList<>();
        while(!this.isOver()){
            remaining.add(this.next().get());
        }
        return remaining;
    }
}

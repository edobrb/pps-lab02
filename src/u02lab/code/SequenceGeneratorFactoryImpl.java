package u02lab.code;

public abstract class SequenceGeneratorFactoryImpl extends  SequenceGeneratorFactory{
    @Override
    public SequenceGenerator newRandomSequence(final int sequenceLength) {
        return new RandomGenerator(sequenceLength);
    }
    @Override
    public SequenceGenerator newRangeSequence(final int start, final int stop) {
        return new RangeGenerator(start, stop);
    }
}

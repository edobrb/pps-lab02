package u02lab.code;

import static org.junit.Assert.*;

public class FixedLengthGeneratorTest {

    protected void assertOver(final FixedLengthGenerator generator) {
        assertTrue(generator.isOver());
        assertFalse(generator.next().isPresent());
    }

    protected void assertNotOver(final FixedLengthGenerator generator) {
        assertFalse(generator.isOver());
        assertTrue(generator.next().isPresent());
    }
}
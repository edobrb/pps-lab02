package u02lab.code;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class RangeGeneratorTest extends FixedLengthGeneratorTest {

    private static final int START = 0;
    private static final int STOP = 100;

    @Test(expected = IllegalArgumentException.class)
    public void constructorArgumentException() {
        final RangeGenerator generator = new RangeGenerator(STOP, START);
    }

    @Test
    public void singleValue() {
        final RangeGenerator generator = new RangeGenerator(START, START);
        assertEquals(new Integer(START), generator.next().get());
        this.assertOver(generator);
    }

    @Test
    public void multipleValue() {
        final RangeGenerator generator = new RangeGenerator(START, STOP);
        for (int i = START; i <= STOP; i++) {
            assertEquals(new Integer(i), generator.next().get());
        }
        this.assertOver(generator);
    }

    @Test
    public void multipleValueAndReset() {
        final RangeGenerator generator = new RangeGenerator(START, STOP);
        for (int i = START; i <= STOP ; i++) {
            assertEquals(new Integer(i), generator.next().get());
        }
        this.assertOver(generator);
        generator.reset();
        for (int i = START; i <= STOP - 1; i++) {
            assertEquals(new Integer(i), generator.next().get());
        }
        this.assertNotOver(generator);
    }

    @Test
    public void remainingValues() {
        final RangeGenerator generator = new RangeGenerator(START, STOP);
        generator.next(); //skip one
        final List<Integer> remaining = generator.allRemaining();
        assertEquals((STOP - START + 1) - 1, remaining.size());
        int index = 0;
        for (int i = START + 1; i <= STOP; i++) {
            assertEquals(new Integer(i), remaining.get(index++));
        }
        this.assertOver(generator);
    }


}
package u02lab.code;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class RandomGeneratorTest extends FixedLengthGeneratorTest {

    private final static long SEED = 1;

    @Test
    public void singleValue() {
        final RandomGenerator generator = new RandomGenerator(1, SEED);
        generator.next();
        this.assertOver(generator);
    }

    @Test
    public void multipleValue() {
        final int n = 10000;
        final RandomGenerator generator = new RandomGenerator(n, SEED);
        final List<Integer> remaining = generator.allRemaining();
        long ones = remaining.stream().filter(i -> i == 1).count();
        long zeros = remaining.stream().filter(i -> i == 0).count();
        assertEquals(n,ones + zeros);
        this.assertOver(generator);
    }

    @Test
    public void sequenceOfValues() {
        final RandomGenerator generator = new RandomGenerator(0, SEED);
        final int maxSequenceLength = 20;
        for(int i = 0; i < maxSequenceLength; i++){
            this.assertExistElementsInSequence(i, 0, generator);
            this.assertExistElementsInSequence(i, 1, generator);
        }
    }

    @Test
    public void sameSequenceTest() {
        final int n = 2000;
        final RandomGenerator generator = new RandomGenerator(n, SEED);
        final List<Integer> remaining1 = generator.allRemaining();
        generator.reset();
        final List<Integer> remaining2 = generator.allRemaining();
        assertArrayEquals(remaining1.toArray(), remaining2.toArray());
    }

    private void assertExistElementsInSequence(final int count, final int x, RandomGenerator generator){
        int inSequence = 0;
        int seedOffset = 0;
        while(true){

            if(generator.isOver()){
                seedOffset++;
                generator = new RandomGenerator(2147483647, SEED + seedOffset);
            }

            final int v = generator.next().get();
            if (v == x) {
                inSequence++;
            } else {
                inSequence = 0;
            }

            if (inSequence >= count) {
                return;
            }
        }
    }

}